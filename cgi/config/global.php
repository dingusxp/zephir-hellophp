<?php

$config = [
    'name' => 'hsbphp',
];

// 加载本地配置
$localConfigFile = dirname(__FILE__).'/_global.php';
if (is_file($localConfigFile)) {
    $localConfig = include($localConfigFile);
    $config = array_merge($config, (array)$localConfig);
}

return $config;
