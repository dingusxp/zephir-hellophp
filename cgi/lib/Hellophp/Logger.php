<?php

namespace Hellophp;

class Logger {

    private static function _getId() {
        
        if (empty($_SERVER['HTTP_INVOKE_ID'])) {
            $logId = sprintf('%10d%06d%03d', time(), getmypid(), mt_rand(0, 999));
            $_SERVER['HTTP_INVOKE_ID'] = $logId;
        }
        return $_SERVER['HTTP_INVOKE_ID'];
    }

    public static function log($message) {

        $date = date('[Y-m-d H:i:s]');
        $microtime = strstr(microtime(true), '.');
        $content = sprintf("%s%s - [%s] %s\n", $date, $microtime, self::_getId(), $message);
        $logFile = Config::get('log.dir', APP_PATH.'/log').'/'.date('Y-m-d').'.log';
        if (!is_dir(dirname($logFile))) {
            mkdir(dirname($logFile), 0777, true);
        }
        file_put_contents($logFile, $content, FILE_APPEND | LOCK_EX);
    }
}