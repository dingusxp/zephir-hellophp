<?php

namespace Hellophp;

class Config {

    private static $_dir = null;
    private static $_config = [];

    private static function _init() {
        
        if (null !== self::$_dir) {
            return;
        }
        self::$_dir = defined('CONFIG_DIR') ? CONFIG_DIR : APP_PATH.'/config';
        if (is_file(self::$_dir.'/global.php')) {
            self::loadConfig(include(self::$_dir.'/global.php'));
        }
    }

    public static function loadConfig($data, $prefix = '') {

        if (!is_array($data)) {
            return;
        }
        foreach($data as $key => $value) {
            $key = $prefix . $key;
            if (!array_key_exists($key, self::$_config)) {
                self::$_config[$key] = $value;
            }
            if (is_array($value)) {
                self::loadConfig($value, $key . '.');
            }
        }
    }

    public static function get($key, $default = null) {

        self::_init();
        if (array_key_exists($key, self::$_config)) {
            return self::$_config[$key];
        }

        // 尝试自动加载配置文件
        $parts = explode('.', $key);
        $path = self::$_dir;
        $prefix = '';
        for($i = 0, $len = count($parts); $i < $len; $i++) {
            $prefix .= ($i == 0 ? $parts[$i] : '.' . $parts[$i]);
            $path .= '/' . $parts[$i];
            if (is_file($path . '.php')) {
                $data = include($path . '.php');
                if (is_array($data)) {
                    self::$_config[$prefix] = $data;
                    self::loadConfig($data, $prefix . '.');
                }
            }
        }

        // 若还是没有取到，返回默认值
        if (array_key_exists($key, self::$_config)) {
            return self::$_config[$key];
        }
        return $default;
    }
}
