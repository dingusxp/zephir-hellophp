<?php

define('APP_PATH', __DIR__);
define('CONFIG_DIR', APP_PATH.'/config');

class Controller {

    public function __construct() {

        if (!class_exists('Hellophp\Logger')) {
            include APP_PATH.'/lib/Hellophp/loader.php';
        }
        Hellophp\Logger::log('start');
    }

    public function __destruct() {
        
        Hellophp\Logger::log('end');
    }

    public function actionIndex() {

        $db = new Hellophp\Db();
        $res = $db->query('select count(*) as cnt from guestbook');
        $cnt = $res[0]['cnt'];
        $rows = $db->query('select * from guestbook order by created_at desc limit 10');
        header('Content-Type: application/json');
        echo json_encode(['total' => $cnt, 'list' => $rows]);
    }
}
(new Controller())->actionIndex();
