# zephir-hellophp
> zephir 编写php扩展练习

### 项目介绍

zephir (https://github.com/zephir-lang) 是一个可以用来更方便编写PHP扩展的语言。

关于 zephir 的基本介绍和使用可以参考： 

https://dingusxp.com/app/blog/view?key=z0ku6ek6q2e8342mscza

本示例主要实现一个简单的示例：Hellophp。该扩展主要包含3个常用类： Config, Logger, Db。


### 使用说明

**建议环境：** centos 7 + php7.3

**准备工作：**

创建数据库 hellophp，导入样例表和数据：

```
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `guestbook`;
CREATE TABLE `guestbook` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'ID',
  `username` varchar(64) NOT NULL DEFAULT '' COMMENT 'guest name',
  `message` varchar(1024) NOT NULL DEFAULT '' COMMENT 'message',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='guestbook list';

INSERT INTO `guestbook` (`id`, `username`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Tom', 'Hello, world', '2021-10-12 00:00:00', '2021-10-12 00:00:00'),
(2, '小明', '你好，世界！', '2021-10-12 00:00:00', '2021-10-12 00:00:00');


ALTER TABLE `guestbook`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_at` (`created_at`);


ALTER TABLE `guestbook`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=3;
```

根据实际数据库配置，修改 cgi/config/db.php 中的数据库连接参数。

**运行原生代码：**

将 cgi 目录挂在 nginx server 下 或者直接 ```php -S 127.0.0.1:8080``` 运行。

浏览器访问即可，正常是输出一个json数据。

**使用扩展：**

zephir/hellophp/ext 目录是编写好的 Hellophp 扩展，按标准 php扩展编译方式安装即可。

安装完成，同上运行即可（此时因扩展内置了类库，不再加载 lib 目录下的代码）。

可以压测比较一下使用扩展前后性能。
```
ab -n 10000 -c 10 http://127.0.0.1:8080/

# 原生
Concurrency Level:      10
Time taken for tests:   20.814 seconds
Complete requests:      10000
Failed requests:        0
Write errors:           0
Total transferred:      4500000 bytes
HTML transferred:       2950000 bytes
Requests per second:    480.44 [#/sec] (mean)
Time per request:       20.814 [ms] (mean)
Time per request:       2.081 [ms] (mean, across all concurrent requests)
Transfer rate:          211.13 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.0      0       0
Processing:     2   21   0.6     21      26
Waiting:        2   21   0.6     21      25
Total:          2   21   0.6     21      26

Percentage of the requests served within a certain time (ms)
  50%     21
  66%     21
  75%     21
  80%     21
  90%     21
  95%     21
  98%     22
  99%     22
 100%     26 (longest request)


# 扩展
Server Hostname:        127.0.0.1
Server Port:            7788

Document Path:          /
Document Length:        295 bytes

Concurrency Level:      10
Time taken for tests:   18.616 seconds
Complete requests:      10000
Failed requests:        0
Write errors:           0
Total transferred:      4500000 bytes
HTML transferred:       2950000 bytes
Requests per second:    537.18 [#/sec] (mean)
Time per request:       18.616 [ms] (mean)
Time per request:       1.862 [ms] (mean, across all concurrent requests)
Transfer rate:          236.06 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.0      0       0
Processing:     2   19   0.6     19      22
Waiting:        2   19   0.6     19      22
Total:          2   19   0.6     19      22

Percentage of the requests served within a certain time (ms)
  50%     19
  66%     19
  75%     19
  80%     19
  90%     19
  95%     19
  98%     20
  99%     20
 100%     22 (longest request)
```

可以看到性能提升仅10%左右。

这主要是因为zephir中各种内存申请和释放都是在 RINIT/RSHUTDOWN 阶段。

好处是发生内存泄漏的可能性大大降低，坏处是不能利用扩展资源随进程常驻的优势。

有感兴趣的，可以编辑 zephir/hellphp/hellophp 代码，自定义PHP 生命周期事件，尝试将 配置文件数组，数据库连接句柄 等资源持久化起来。

**提醒：** 代码仅演示用，不可使用于正式环境。
