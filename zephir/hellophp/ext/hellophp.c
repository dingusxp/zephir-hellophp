
/* This file was generated automatically by Zephir do not modify it! */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <php.h>

#include "php_ext.h"
#include "hellophp.h"

#include <ext/standard/info.h>

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/globals.h"
#include "kernel/main.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"



zend_class_entry *hellophp_config_ce;
zend_class_entry *hellophp_db_ce;
zend_class_entry *hellophp_db_exception_ce;
zend_class_entry *hellophp_logger_ce;

ZEND_DECLARE_MODULE_GLOBALS(hellophp)

PHP_INI_BEGIN()
	
PHP_INI_END()

static PHP_MINIT_FUNCTION(hellophp)
{
	REGISTER_INI_ENTRIES();
	zephir_module_init();
	ZEPHIR_INIT(Hellophp_Config);
	ZEPHIR_INIT(Hellophp_Db);
	ZEPHIR_INIT(Hellophp_Db_Exception);
	ZEPHIR_INIT(Hellophp_Logger);
	
	return SUCCESS;
}

#ifndef ZEPHIR_RELEASE
static PHP_MSHUTDOWN_FUNCTION(hellophp)
{
	
	zephir_deinitialize_memory(TSRMLS_C);
	UNREGISTER_INI_ENTRIES();
	return SUCCESS;
}
#endif

/**
 * Initialize globals on each request or each thread started
 */
static void php_zephir_init_globals(zend_hellophp_globals *hellophp_globals TSRMLS_DC)
{
	hellophp_globals->initialized = 0;

	/* Cache Enabled */
	hellophp_globals->cache_enabled = 1;

	/* Recursive Lock */
	hellophp_globals->recursive_lock = 0;

	/* Static cache */
	memset(hellophp_globals->scache, '\0', sizeof(zephir_fcall_cache_entry*) * ZEPHIR_MAX_CACHE_SLOTS);

	
	
}

/**
 * Initialize globals only on each thread started
 */
static void php_zephir_init_module_globals(zend_hellophp_globals *hellophp_globals TSRMLS_DC)
{
	
}

static PHP_RINIT_FUNCTION(hellophp)
{
	zend_hellophp_globals *hellophp_globals_ptr;
	hellophp_globals_ptr = ZEPHIR_VGLOBAL;

	php_zephir_init_globals(hellophp_globals_ptr);
	zephir_initialize_memory(hellophp_globals_ptr);

		zephir_init_static_properties_Hellophp_Config(TSRMLS_C);
		zephir_init_static_properties_Hellophp_Db(TSRMLS_C);
	
	return SUCCESS;
}

static PHP_RSHUTDOWN_FUNCTION(hellophp)
{
	
	zephir_deinitialize_memory(TSRMLS_C);
	return SUCCESS;
}



static PHP_MINFO_FUNCTION(hellophp)
{
	php_info_print_box_start(0);
	php_printf("%s", PHP_HELLOPHP_DESCRIPTION);
	php_info_print_box_end();

	php_info_print_table_start();
	php_info_print_table_header(2, PHP_HELLOPHP_NAME, "enabled");
	php_info_print_table_row(2, "Author", PHP_HELLOPHP_AUTHOR);
	php_info_print_table_row(2, "Version", PHP_HELLOPHP_VERSION);
	php_info_print_table_row(2, "Build Date", __DATE__ " " __TIME__ );
	php_info_print_table_row(2, "Powered by Zephir", "Version " PHP_HELLOPHP_ZEPVERSION);
	php_info_print_table_end();
	
	DISPLAY_INI_ENTRIES();
}

static PHP_GINIT_FUNCTION(hellophp)
{
#if defined(COMPILE_DL_HELLOPHP) && defined(ZTS)
	ZEND_TSRMLS_CACHE_UPDATE();
#endif

	php_zephir_init_globals(hellophp_globals);
	php_zephir_init_module_globals(hellophp_globals);
}

static PHP_GSHUTDOWN_FUNCTION(hellophp)
{
	
}


zend_function_entry php_hellophp_functions[] = {
	ZEND_FE_END

};

static const zend_module_dep php_hellophp_deps[] = {
	
	ZEND_MOD_END
};

zend_module_entry hellophp_module_entry = {
	STANDARD_MODULE_HEADER_EX,
	NULL,
	php_hellophp_deps,
	PHP_HELLOPHP_EXTNAME,
	php_hellophp_functions,
	PHP_MINIT(hellophp),
#ifndef ZEPHIR_RELEASE
	PHP_MSHUTDOWN(hellophp),
#else
	NULL,
#endif
	PHP_RINIT(hellophp),
	PHP_RSHUTDOWN(hellophp),
	PHP_MINFO(hellophp),
	PHP_HELLOPHP_VERSION,
	ZEND_MODULE_GLOBALS(hellophp),
	PHP_GINIT(hellophp),
	PHP_GSHUTDOWN(hellophp),
#ifdef ZEPHIR_POST_REQUEST
	PHP_PRSHUTDOWN(hellophp),
#else
	NULL,
#endif
	STANDARD_MODULE_PROPERTIES_EX
};

/* implement standard "stub" routine to introduce ourselves to Zend */
#ifdef COMPILE_DL_HELLOPHP
# ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE()
# endif
ZEND_GET_MODULE(hellophp)
#endif
