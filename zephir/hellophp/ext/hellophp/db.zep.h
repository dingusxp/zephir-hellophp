
extern zend_class_entry *hellophp_db_ce;

ZEPHIR_INIT_CLASS(Hellophp_Db);

PHP_METHOD(Hellophp_Db, __construct);
PHP_METHOD(Hellophp_Db, _getDbh);
PHP_METHOD(Hellophp_Db, query);
void zephir_init_static_properties_Hellophp_Db(TSRMLS_D);

ZEND_BEGIN_ARG_INFO_EX(arginfo_hellophp_db___construct, 0, 0, 0)
	ZEND_ARG_INFO(0, instanceName)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_hellophp_db__getdbh, 0, 0, 1)
	ZEND_ARG_INFO(0, instanceName)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_hellophp_db_query, 0, 0, 1)
	ZEND_ARG_INFO(0, query)
	ZEND_ARG_INFO(0, params)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(hellophp_db_method_entry) {
	PHP_ME(Hellophp_Db, __construct, arginfo_hellophp_db___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
	PHP_ME(Hellophp_Db, _getDbh, arginfo_hellophp_db__getdbh, ZEND_ACC_PRIVATE|ZEND_ACC_STATIC)
	PHP_ME(Hellophp_Db, query, arginfo_hellophp_db_query, ZEND_ACC_PUBLIC)
	PHP_FE_END
};
