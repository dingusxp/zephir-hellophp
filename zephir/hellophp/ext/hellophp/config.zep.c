
#ifdef HAVE_CONFIG_H
#include "../ext_config.h"
#endif

#include <php.h>
#include "../php_ext.h"
#include "../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/operators.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/concat.h"
#include "kernel/require.h"
#include "kernel/array.h"
#include "kernel/string.h"


ZEPHIR_INIT_CLASS(Hellophp_Config) {

	ZEPHIR_REGISTER_CLASS(Hellophp, Config, hellophp, config, hellophp_config_method_entry, 0);

	zend_declare_property_null(hellophp_config_ce, SL("_dir"), ZEND_ACC_PRIVATE|ZEND_ACC_STATIC);

	zend_declare_property_null(hellophp_config_ce, SL("_config"), ZEND_ACC_PRIVATE|ZEND_ACC_STATIC);

	return SUCCESS;

}

PHP_METHOD(Hellophp_Config, _init) {

	zval _0, _1, _2, _3, _4, _5, _6, _7, _8$$4, _9$$4, _10$$4;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_6);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8$$4);
	ZVAL_UNDEF(&_9$$4);
	ZVAL_UNDEF(&_10$$4);

	ZEPHIR_MM_GROW();

	zephir_read_static_property_ce(&_0, hellophp_config_ce, SL("_dir"), PH_NOISY_CC | PH_READONLY);
	if (Z_TYPE_P(&_0) != IS_NULL) {
		RETURN_MM_NULL();
	}
	ZEPHIR_INIT_VAR(&_1);
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, "CONFIG_DIR");
	ZEPHIR_CALL_FUNCTION(&_3, "defined", NULL, 1, &_2);
	zephir_check_call_status();
	if (zephir_is_true(&_3)) {
		ZEPHIR_INIT_NVAR(&_1);
		ZEPHIR_GET_CONSTANT(&_1, "CONFIG_DIR");
	} else {
		ZEPHIR_INIT_VAR(&_4);
		ZEPHIR_INIT_NVAR(&_2);
		ZVAL_STRING(&_2, "APP_PATH");
		ZEPHIR_CALL_FUNCTION(&_5, "defined", NULL, 1, &_2);
		zephir_check_call_status();
		if (zephir_is_true(&_5)) {
			ZEPHIR_INIT_NVAR(&_4);
			ZEPHIR_GET_CONSTANT(&_4, "APP_APTH");
		} else {
			ZEPHIR_INIT_NVAR(&_4);
			ZVAL_STRING(&_4, ".");
		}
		ZEPHIR_INIT_NVAR(&_1);
		ZEPHIR_CONCAT_VS(&_1, &_4, "/config");
	}
	zephir_update_static_property_ce(hellophp_config_ce, ZEND_STRL("_dir"), &_1);
	ZEPHIR_OBS_VAR(&_6);
	zephir_read_static_property_ce(&_6, hellophp_config_ce, SL("_dir"), PH_NOISY_CC);
	ZEPHIR_INIT_NVAR(&_1);
	ZEPHIR_CONCAT_VS(&_1, &_6, "/global.php");
	ZEPHIR_CALL_FUNCTION(&_7, "is_file", NULL, 2, &_1);
	zephir_check_call_status();
	if (zephir_is_true(&_7)) {
		ZEPHIR_OBS_VAR(&_8$$4);
		zephir_read_static_property_ce(&_8$$4, hellophp_config_ce, SL("_dir"), PH_NOISY_CC);
		ZEPHIR_INIT_VAR(&_9$$4);
		ZEPHIR_CONCAT_VS(&_9$$4, &_8$$4, "/global.php");
		ZEPHIR_OBSERVE_OR_NULLIFY_PPZV(&_10$$4);
		if (zephir_require_zval_ret(&_10$$4, &_9$$4) == FAILURE) {
			RETURN_MM_NULL();
		}
		ZEPHIR_CALL_SELF(NULL, "loadconfig", NULL, 0, &_10$$4);
		zephir_check_call_status();
	}
	ZEPHIR_MM_RESTORE();

}

PHP_METHOD(Hellophp_Config, loadConfig) {

	zend_string *_4;
	zend_ulong _3;
	zend_bool _0;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zephir_fcall_cache_entry *_7 = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *data = NULL, data_sub, *prefix = NULL, prefix_sub, key, value, *_1, _2, _5$$4, _6$$4, _8$$6, _9$$7, _10$$7, _11$$9;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&data_sub);
	ZVAL_UNDEF(&prefix_sub);
	ZVAL_UNDEF(&key);
	ZVAL_UNDEF(&value);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_5$$4);
	ZVAL_UNDEF(&_6$$4);
	ZVAL_UNDEF(&_8$$6);
	ZVAL_UNDEF(&_9$$7);
	ZVAL_UNDEF(&_10$$7);
	ZVAL_UNDEF(&_11$$9);

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &data, &prefix);

	if (!prefix) {
		prefix = &prefix_sub;
		ZEPHIR_INIT_VAR(prefix);
		ZVAL_STRING(prefix, "");
	}


	_0 = !zephir_is_true(data);
	if (!(_0)) {
		_0 = !(Z_TYPE_P(data) == IS_ARRAY);
	}
	if (_0) {
		RETURN_MM_NULL();
	}
	zephir_is_iterable(data, 0, "hellophp/Config.zep", 34);
	if (Z_TYPE_P(data) == IS_ARRAY) {
		ZEND_HASH_FOREACH_KEY_VAL(Z_ARRVAL_P(data), _3, _4, _1)
		{
			ZEPHIR_INIT_NVAR(&key);
			if (_4 != NULL) { 
				ZVAL_STR_COPY(&key, _4);
			} else {
				ZVAL_LONG(&key, _3);
			}
			ZEPHIR_INIT_NVAR(&value);
			ZVAL_COPY(&value, _1);
			ZEPHIR_INIT_NVAR(&_5$$4);
			ZEPHIR_CONCAT_VV(&_5$$4, prefix, &key);
			ZEPHIR_CPY_WRT(&key, &_5$$4);
			zephir_read_static_property_ce(&_6$$4, hellophp_config_ce, SL("_config"), PH_NOISY_CC | PH_READONLY);
			if (!(zephir_array_key_exists(&_6$$4, &key))) {
				zephir_update_static_property_array_multi_ce(hellophp_config_ce, SL("_config"), &value, SL("z"), 1, &key);
			}
			if (Z_TYPE_P(&value) == IS_ARRAY) {
				ZEPHIR_INIT_NVAR(&_8$$6);
				ZEPHIR_CONCAT_VS(&_8$$6, &key, ".");
				ZEPHIR_CALL_SELF(NULL, "loadconfig", &_7, 0, &value, &_8$$6);
				zephir_check_call_status();
			}
		} ZEND_HASH_FOREACH_END();
	} else {
		ZEPHIR_CALL_METHOD(NULL, data, "rewind", NULL, 0);
		zephir_check_call_status();
		while (1) {
			ZEPHIR_CALL_METHOD(&_2, data, "valid", NULL, 0);
			zephir_check_call_status();
			if (!zend_is_true(&_2)) {
				break;
			}
			ZEPHIR_CALL_METHOD(&key, data, "key", NULL, 0);
			zephir_check_call_status();
			ZEPHIR_CALL_METHOD(&value, data, "current", NULL, 0);
			zephir_check_call_status();
				ZEPHIR_INIT_NVAR(&_9$$7);
				ZEPHIR_CONCAT_VV(&_9$$7, prefix, &key);
				ZEPHIR_CPY_WRT(&key, &_9$$7);
				zephir_read_static_property_ce(&_10$$7, hellophp_config_ce, SL("_config"), PH_NOISY_CC | PH_READONLY);
				if (!(zephir_array_key_exists(&_10$$7, &key))) {
					zephir_update_static_property_array_multi_ce(hellophp_config_ce, SL("_config"), &value, SL("z"), 1, &key);
				}
				if (Z_TYPE_P(&value) == IS_ARRAY) {
					ZEPHIR_INIT_NVAR(&_11$$9);
					ZEPHIR_CONCAT_VS(&_11$$9, &key, ".");
					ZEPHIR_CALL_SELF(NULL, "loadconfig", &_7, 0, &value, &_11$$9);
					zephir_check_call_status();
				}
			ZEPHIR_CALL_METHOD(NULL, data, "next", NULL, 0);
			zephir_check_call_status();
		}
	}
	ZEPHIR_INIT_NVAR(&value);
	ZEPHIR_INIT_NVAR(&key);
	ZEPHIR_MM_RESTORE();

}

PHP_METHOD(Hellophp_Config, get) {

	zend_bool _5;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS, _6, _7;
	zephir_fcall_cache_entry *_0 = NULL, *_14 = NULL, *_17 = NULL;
	zval *key = NULL, key_sub, *defaultValue = NULL, defaultValue_sub, __$null, _1, parts, path, prefix, _4, i, data, _19, _2$$3, _3$$3, _8$$4, _9$$4, _10$$4, _11$$4, _12$$4, _13$$4, _15$$5, _16$$5, _18$$6, _20$$7, _21$$7;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&key_sub);
	ZVAL_UNDEF(&defaultValue_sub);
	ZVAL_NULL(&__$null);
	ZVAL_UNDEF(&_1);
	ZVAL_UNDEF(&parts);
	ZVAL_UNDEF(&path);
	ZVAL_UNDEF(&prefix);
	ZVAL_UNDEF(&_4);
	ZVAL_UNDEF(&i);
	ZVAL_UNDEF(&data);
	ZVAL_UNDEF(&_19);
	ZVAL_UNDEF(&_2$$3);
	ZVAL_UNDEF(&_3$$3);
	ZVAL_UNDEF(&_8$$4);
	ZVAL_UNDEF(&_9$$4);
	ZVAL_UNDEF(&_10$$4);
	ZVAL_UNDEF(&_11$$4);
	ZVAL_UNDEF(&_12$$4);
	ZVAL_UNDEF(&_13$$4);
	ZVAL_UNDEF(&_15$$5);
	ZVAL_UNDEF(&_16$$5);
	ZVAL_UNDEF(&_18$$6);
	ZVAL_UNDEF(&_20$$7);
	ZVAL_UNDEF(&_21$$7);

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &key, &defaultValue);

	if (!defaultValue) {
		defaultValue = &defaultValue_sub;
		defaultValue = &__$null;
	}


	ZEPHIR_CALL_SELF(NULL, "_init", &_0, 3);
	zephir_check_call_status();
	zephir_read_static_property_ce(&_1, hellophp_config_ce, SL("_config"), PH_NOISY_CC | PH_READONLY);
	if (zephir_array_key_exists(&_1, key)) {
		zephir_read_static_property_ce(&_2$$3, hellophp_config_ce, SL("_config"), PH_NOISY_CC | PH_READONLY);
		zephir_array_fetch(&_3$$3, &_2$$3, key, PH_NOISY | PH_READONLY, "hellophp/Config.zep", 40);
		RETURN_CTOR(&_3$$3);
	}
	ZEPHIR_INIT_VAR(&parts);
	zephir_fast_explode_str(&parts, SL("."), key, LONG_MAX);
	ZEPHIR_INIT_VAR(&path);
	ZVAL_STRING(&path, "");
	ZEPHIR_INIT_VAR(&prefix);
	ZVAL_STRING(&prefix, "");
	ZEPHIR_OBS_VAR(&_4);
	zephir_read_static_property_ce(&_4, hellophp_config_ce, SL("_dir"), PH_NOISY_CC);
	zephir_concat_self(&path, &_4);
	_7 = (zephir_fast_count_int(&parts) - 1);
	_6 = 0;
	_5 = 0;
	if (_6 <= _7) {
		while (1) {
			if (_5) {
				_6++;
				if (!(_6 <= _7)) {
					break;
				}
			} else {
				_5 = 1;
			}
			ZEPHIR_INIT_NVAR(&i);
			ZVAL_LONG(&i, _6);
			ZEPHIR_INIT_NVAR(&_8$$4);
			if (ZEPHIR_IS_LONG(&i, 0)) {
				ZEPHIR_OBS_NVAR(&_8$$4);
				zephir_array_fetch(&_8$$4, &parts, &i, PH_NOISY, "hellophp/Config.zep", 51);
			} else {
				zephir_array_fetch(&_9$$4, &parts, &i, PH_NOISY | PH_READONLY, "hellophp/Config.zep", 51);
				ZEPHIR_INIT_NVAR(&_8$$4);
				ZEPHIR_CONCAT_SV(&_8$$4, ".", &_9$$4);
			}
			zephir_concat_self(&prefix, &_8$$4);
			zephir_array_fetch(&_10$$4, &parts, &i, PH_NOISY | PH_READONLY, "hellophp/Config.zep", 52);
			ZEPHIR_INIT_NVAR(&_11$$4);
			ZEPHIR_CONCAT_SV(&_11$$4, "/", &_10$$4);
			zephir_concat_self(&path, &_11$$4);
			ZEPHIR_INIT_NVAR(&_12$$4);
			ZEPHIR_CONCAT_VS(&_12$$4, &path, ".php");
			ZEPHIR_CALL_FUNCTION(&_13$$4, "is_file", &_14, 2, &_12$$4);
			zephir_check_call_status();
			if (zephir_is_true(&_13$$4)) {
				ZEPHIR_INIT_NVAR(&_15$$5);
				ZEPHIR_CONCAT_VS(&_15$$5, &path, ".php");
				ZEPHIR_OBSERVE_OR_NULLIFY_PPZV(&_16$$5);
				if (zephir_require_zval_ret(&_16$$5, &_15$$5) == FAILURE) {
					RETURN_MM_NULL();
				}
				ZEPHIR_CPY_WRT(&data, &_16$$5);
				if (Z_TYPE_P(&data) == IS_ARRAY) {
					zephir_update_static_property_array_multi_ce(hellophp_config_ce, SL("_config"), &data, SL("z"), 1, &prefix);
					ZEPHIR_INIT_NVAR(&_18$$6);
					ZEPHIR_CONCAT_VS(&_18$$6, &prefix, ".");
					ZEPHIR_CALL_SELF(NULL, "loadconfig", &_17, 0, &data, &_18$$6);
					zephir_check_call_status();
				}
			}
		}
	}
	zephir_read_static_property_ce(&_19, hellophp_config_ce, SL("_config"), PH_NOISY_CC | PH_READONLY);
	if (zephir_array_key_exists(&_19, key)) {
		zephir_read_static_property_ce(&_20$$7, hellophp_config_ce, SL("_config"), PH_NOISY_CC | PH_READONLY);
		zephir_array_fetch(&_21$$7, &_20$$7, key, PH_NOISY | PH_READONLY, "hellophp/Config.zep", 64);
		RETURN_CTOR(&_21$$7);
	}
	RETVAL_ZVAL(defaultValue, 1, 0);
	RETURN_MM();

}

void zephir_init_static_properties_Hellophp_Config(TSRMLS_D) {

	zval _0;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
		ZVAL_UNDEF(&_0);

	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_0);
	array_init(&_0);
	zephir_update_static_property_ce(hellophp_config_ce, ZEND_STRL("_config"), &_0);
	ZEPHIR_MM_RESTORE();

}

