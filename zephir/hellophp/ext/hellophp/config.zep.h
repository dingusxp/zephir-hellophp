
extern zend_class_entry *hellophp_config_ce;

ZEPHIR_INIT_CLASS(Hellophp_Config);

PHP_METHOD(Hellophp_Config, _init);
PHP_METHOD(Hellophp_Config, loadConfig);
PHP_METHOD(Hellophp_Config, get);
void zephir_init_static_properties_Hellophp_Config(TSRMLS_D);

ZEND_BEGIN_ARG_INFO_EX(arginfo_hellophp_config_loadconfig, 0, 0, 1)
	ZEND_ARG_INFO(0, data)
	ZEND_ARG_INFO(0, prefix)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_hellophp_config_get, 0, 0, 1)
	ZEND_ARG_INFO(0, key)
	ZEND_ARG_INFO(0, defaultValue)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(hellophp_config_method_entry) {
	PHP_ME(Hellophp_Config, _init, NULL, ZEND_ACC_PRIVATE|ZEND_ACC_STATIC)
	PHP_ME(Hellophp_Config, loadConfig, arginfo_hellophp_config_loadconfig, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_ME(Hellophp_Config, get, arginfo_hellophp_config_get, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_FE_END
};
