
#ifdef HAVE_CONFIG_H
#include "../ext_config.h"
#endif

#include <php.h>
#include "../php_ext.h"
#include "../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/array.h"
#include "kernel/memory.h"
#include "kernel/time.h"
#include "kernel/fcall.h"
#include "kernel/math.h"
#include "kernel/operators.h"
#include "kernel/object.h"
#include "kernel/concat.h"


ZEPHIR_INIT_CLASS(Hellophp_Logger) {

	ZEPHIR_REGISTER_CLASS(Hellophp, Logger, hellophp, logger, hellophp_logger_method_entry, 0);

	return SUCCESS;

}

PHP_METHOD(Hellophp_Logger, _getId) {

	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval _SERVER, _0, _7, logId$$3, _1$$3, _2$$3, _3$$3, _4$$3, _5$$3, _6$$3;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&_SERVER);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&logId$$3);
	ZVAL_UNDEF(&_1$$3);
	ZVAL_UNDEF(&_2$$3);
	ZVAL_UNDEF(&_3$$3);
	ZVAL_UNDEF(&_4$$3);
	ZVAL_UNDEF(&_5$$3);
	ZVAL_UNDEF(&_6$$3);

	ZEPHIR_MM_GROW();
	zephir_get_global(&_SERVER, SL("_SERVER"));

	ZEPHIR_INIT_VAR(&_0);
	ZVAL_STRING(&_0, "HTTP_INVOKE_ID");
	if (!(zephir_array_key_exists(&_SERVER, &_0))) {
		ZEPHIR_INIT_VAR(&_1$$3);
		zephir_time(&_1$$3);
		ZEPHIR_CALL_FUNCTION(&_2$$3, "getmypid", NULL, 7);
		zephir_check_call_status();
		ZVAL_LONG(&_3$$3, 0);
		ZVAL_LONG(&_4$$3, 999);
		ZEPHIR_INIT_VAR(&_5$$3);
		ZVAL_STRING(&_5$$3, "%10d%06d%03d");
		ZVAL_LONG(&_6$$3, zephir_mt_rand(zephir_get_intval(&_3$$3), zephir_get_intval(&_4$$3)));
		ZEPHIR_CALL_FUNCTION(&logId$$3, "sprintf", NULL, 5, &_5$$3, &_1$$3, &_2$$3, &_6$$3);
		zephir_check_call_status();
		zephir_array_update_string(&_SERVER, SL("HTTP_INVOKE_ID"), &logId$$3, PH_COPY | PH_SEPARATE);
	}
	zephir_array_fetch_string(&_7, &_SERVER, SL("HTTP_INVOKE_ID"), PH_NOISY | PH_READONLY, "hellophp/Logger.zep", 12);
	RETURN_CTOR(&_7);

}

PHP_METHOD(Hellophp_Logger, log) {

	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zephir_fcall_cache_entry *_1 = NULL, *_4 = NULL, *_6 = NULL, *_11 = NULL;
	zval *message = NULL, message_sub, __$true, date, microtime, content, logFile, _0, _2, _3, _5, _7, _8, _9, _10, _12, _15, _13$$3, _14$$3;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&message_sub);
	ZVAL_BOOL(&__$true, 1);
	ZVAL_UNDEF(&date);
	ZVAL_UNDEF(&microtime);
	ZVAL_UNDEF(&content);
	ZVAL_UNDEF(&logFile);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_2);
	ZVAL_UNDEF(&_3);
	ZVAL_UNDEF(&_5);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&_9);
	ZVAL_UNDEF(&_10);
	ZVAL_UNDEF(&_12);
	ZVAL_UNDEF(&_15);
	ZVAL_UNDEF(&_13$$3);
	ZVAL_UNDEF(&_14$$3);

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &message);



	ZEPHIR_INIT_VAR(&_0);
	ZVAL_STRING(&_0, "[Y-m-d H:i:s]");
	ZEPHIR_CALL_FUNCTION(&date, "date", &_1, 8, &_0);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_0);
	zephir_microtime(&_0, &__$true);
	ZEPHIR_INIT_VAR(&_2);
	ZVAL_STRING(&_2, ".");
	ZEPHIR_CALL_FUNCTION(&microtime, "strstr", NULL, 9, &_0, &_2);
	zephir_check_call_status();
	ZEPHIR_CALL_SELF(&_3, "_getid", &_4, 10);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZVAL_STRING(&_2, "%s%s - [%s] %s\n");
	ZEPHIR_CALL_FUNCTION(&content, "sprintf", NULL, 5, &_2, &date, &microtime, &_3, message);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_2);
	ZEPHIR_GET_CONSTANT(&_2, "APP_PATH");
	ZEPHIR_INIT_VAR(&_7);
	ZEPHIR_CONCAT_VS(&_7, &_2, "/log");
	ZEPHIR_INIT_VAR(&_8);
	ZVAL_STRING(&_8, "log.dir");
	ZEPHIR_CALL_CE_STATIC(&_5, hellophp_config_ce, "get", &_6, 0, &_8, &_7);
	zephir_check_call_status();
	ZEPHIR_INIT_NVAR(&_8);
	ZVAL_STRING(&_8, "Y-m-d");
	ZEPHIR_CALL_FUNCTION(&_9, "date", &_1, 8, &_8);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&logFile);
	ZEPHIR_CONCAT_VSVS(&logFile, &_5, "/", &_9, ".log");
	ZEPHIR_CALL_FUNCTION(&_10, "dirname", &_11, 11, &logFile);
	zephir_check_call_status();
	ZEPHIR_CALL_FUNCTION(&_12, "is_dir", NULL, 12, &_10);
	zephir_check_call_status();
	if (!zephir_is_true(&_12)) {
		ZEPHIR_CALL_FUNCTION(&_13$$3, "dirname", &_11, 11, &logFile);
		zephir_check_call_status();
		ZVAL_LONG(&_14$$3, 0777);
		ZEPHIR_CALL_FUNCTION(NULL, "mkdir", NULL, 13, &_13$$3, &_14$$3, &__$true);
		zephir_check_call_status();
	}
	ZVAL_LONG(&_15, (8 | 2));
	ZEPHIR_CALL_FUNCTION(NULL, "file_put_contents", NULL, 14, &logFile, &content, &_15);
	zephir_check_call_status();
	ZEPHIR_MM_RESTORE();

}

