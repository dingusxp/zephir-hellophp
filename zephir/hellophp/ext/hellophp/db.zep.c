
#ifdef HAVE_CONFIG_H
#include "../ext_config.h"
#endif

#include <php.h>
#include "../php_ext.h"
#include "../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/object.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/array.h"
#include "kernel/time.h"
#include "kernel/operators.h"
#include "kernel/concat.h"
#include "ext/pdo/php_pdo_driver.h"
#include "kernel/exception.h"
#include "kernel/string.h"


ZEPHIR_INIT_CLASS(Hellophp_Db) {

	ZEPHIR_REGISTER_CLASS(Hellophp, Db, hellophp, db, hellophp_db_method_entry, 0);

	zend_declare_property_null(hellophp_db_ce, SL("_instances"), ZEND_ACC_PRIVATE|ZEND_ACC_STATIC);

	zend_declare_property_null(hellophp_db_ce, SL("_instancesTimeout"), ZEND_ACC_PRIVATE|ZEND_ACC_STATIC);

	zend_declare_property_null(hellophp_db_ce, SL("_dbh"), ZEND_ACC_PRIVATE);

	zend_declare_property_null(hellophp_db_ce, SL("_instanceName"), ZEND_ACC_PRIVATE);

	zephir_declare_class_constant_long(hellophp_db_ce, SL("INSTANCE_MAX_LIFETIME"), 60);

	return SUCCESS;

}

PHP_METHOD(Hellophp_Db, __construct) {

	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zephir_fcall_cache_entry *_1 = NULL;
	zval *instanceName = NULL, instanceName_sub, _0;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&instanceName_sub);
	ZVAL_UNDEF(&_0);

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 0, 1, &instanceName);

	if (!instanceName) {
		instanceName = &instanceName_sub;
		ZEPHIR_INIT_VAR(instanceName);
		ZVAL_STRING(instanceName, "default");
	}


	zephir_update_property_zval(this_ptr, ZEND_STRL("_instanceName"), instanceName);
	ZEPHIR_CALL_SELF(&_0, "_getdbh", &_1, 4, instanceName);
	zephir_check_call_status();
	zephir_update_property_zval(this_ptr, ZEND_STRL("_dbh"), &_0);
	ZEPHIR_MM_RESTORE();

}

PHP_METHOD(Hellophp_Db, _getDbh) {

	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zephir_fcall_cache_entry *_7 = NULL, *_15 = NULL;
	zval *instanceName = NULL, instanceName_sub, __$false, _0, option, attr, _8, e, dbh, _19, _27, _28, _29, _30, _1$$3, _2$$3, _3$$3, _6$$3, _4$$4, _5$$4, _9$$5, _10$$5, _11$$5, _12$$5, _16$$5, _17$$5, _18$$5, _13$$6, _14$$6, _20$$7, _21$$7, _22$$7, _23$$7, _24$$7, _25$$7, _26$$7;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&instanceName_sub);
	ZVAL_BOOL(&__$false, 0);
	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&option);
	ZVAL_UNDEF(&attr);
	ZVAL_UNDEF(&_8);
	ZVAL_UNDEF(&e);
	ZVAL_UNDEF(&dbh);
	ZVAL_UNDEF(&_19);
	ZVAL_UNDEF(&_27);
	ZVAL_UNDEF(&_28);
	ZVAL_UNDEF(&_29);
	ZVAL_UNDEF(&_30);
	ZVAL_UNDEF(&_1$$3);
	ZVAL_UNDEF(&_2$$3);
	ZVAL_UNDEF(&_3$$3);
	ZVAL_UNDEF(&_6$$3);
	ZVAL_UNDEF(&_4$$4);
	ZVAL_UNDEF(&_5$$4);
	ZVAL_UNDEF(&_9$$5);
	ZVAL_UNDEF(&_10$$5);
	ZVAL_UNDEF(&_11$$5);
	ZVAL_UNDEF(&_12$$5);
	ZVAL_UNDEF(&_16$$5);
	ZVAL_UNDEF(&_17$$5);
	ZVAL_UNDEF(&_18$$5);
	ZVAL_UNDEF(&_13$$6);
	ZVAL_UNDEF(&_14$$6);
	ZVAL_UNDEF(&_20$$7);
	ZVAL_UNDEF(&_21$$7);
	ZVAL_UNDEF(&_22$$7);
	ZVAL_UNDEF(&_23$$7);
	ZVAL_UNDEF(&_24$$7);
	ZVAL_UNDEF(&_25$$7);
	ZVAL_UNDEF(&_26$$7);

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &instanceName);



	zephir_read_static_property_ce(&_0, hellophp_db_ce, SL("_instances"), PH_NOISY_CC | PH_READONLY);
	if (zephir_array_key_exists(&_0, instanceName)) {
		zephir_read_static_property_ce(&_1$$3, hellophp_db_ce, SL("_instancesTimeout"), PH_NOISY_CC | PH_READONLY);
		zephir_array_fetch(&_2$$3, &_1$$3, instanceName, PH_NOISY | PH_READONLY, "hellophp/Db.zep", 21);
		ZEPHIR_INIT_VAR(&_3$$3);
		zephir_time(&_3$$3);
		if (ZEPHIR_GT(&_2$$3, &_3$$3)) {
			zephir_read_static_property_ce(&_4$$4, hellophp_db_ce, SL("_instances"), PH_NOISY_CC | PH_READONLY);
			zephir_array_fetch(&_5$$4, &_4$$4, instanceName, PH_NOISY | PH_READONLY, "hellophp/Db.zep", 22);
			RETURN_CTOR(&_5$$4);
		}
		zephir_read_static_property_ce(&_6$$3, hellophp_db_ce, SL("_instances"), PH_NOISY_CC | PH_READONLY);
		zephir_array_unset(&_6$$3, instanceName, PH_SEPARATE);
	}
	ZEPHIR_INIT_VAR(&_8);
	ZEPHIR_CONCAT_SV(&_8, "db.", instanceName);
	ZEPHIR_CALL_CE_STATIC(&option, hellophp_config_ce, "get", &_7, 0, &_8);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(&attr);
	zephir_create_array(&attr, 3, 0);
	add_index_long(&attr, 3, 2);
	zephir_array_update_long(&attr, 17, &__$false, PH_COPY ZEPHIR_DEBUG_PARAMS_DUMMY);
	zephir_array_update_long(&attr, 20, &__$false, PH_COPY ZEPHIR_DEBUG_PARAMS_DUMMY);

	/* try_start_1: */

		ZEPHIR_INIT_VAR(&dbh);
		object_init_ex(&dbh, php_pdo_get_dbh_ce());
		zephir_array_fetch_string(&_9$$5, &option, SL("dsn"), PH_NOISY | PH_READONLY, "hellophp/Db.zep", 37);
		zephir_array_fetch_string(&_10$$5, &option, SL("user"), PH_NOISY | PH_READONLY, "hellophp/Db.zep", 37);
		zephir_array_fetch_string(&_11$$5, &option, SL("password"), PH_NOISY | PH_READONLY, "hellophp/Db.zep", 37);
		ZEPHIR_CALL_METHOD(NULL, &dbh, "__construct", NULL, 0, &_9$$5, &_10$$5, &_11$$5, &attr);
		zephir_check_call_status_or_jump(try_end_1);
		ZEPHIR_OBS_VAR(&_12$$5);
		zephir_array_fetch_string(&_12$$5, &option, SL("charset"), PH_NOISY, "hellophp/Db.zep", 38);
		if (!(ZEPHIR_IS_EMPTY(&_12$$5))) {
			zephir_array_fetch_string(&_13$$6, &option, SL("charset"), PH_NOISY | PH_READONLY, "hellophp/Db.zep", 39);
			ZEPHIR_INIT_VAR(&_14$$6);
			ZEPHIR_CONCAT_SV(&_14$$6, "SET NAMES ", &_13$$6);
			ZEPHIR_CALL_METHOD(NULL, &dbh, "exec", NULL, 0, &_14$$6);
			zephir_check_call_status_or_jump(try_end_1);
		}
		zephir_array_fetch_string(&_16$$5, &option, SL("dsn"), PH_NOISY | PH_READONLY, "hellophp/Db.zep", 41);
		ZEPHIR_INIT_VAR(&_17$$5);
		ZVAL_STRING(&_17$$5, "connect to %s succeed.");
		ZEPHIR_CALL_FUNCTION(&_18$$5, "sprintf", NULL, 5, &_17$$5, &_16$$5);
		zephir_check_call_status_or_jump(try_end_1);
		ZEPHIR_CALL_CE_STATIC(NULL, hellophp_logger_ce, "log", &_15, 0, &_18$$5);
		zephir_check_call_status_or_jump(try_end_1);

	try_end_1:

	if (EG(exception)) {
		ZEPHIR_INIT_VAR(&_19);
		ZVAL_OBJ(&_19, EG(exception));
		Z_ADDREF_P(&_19);
		if (zephir_instance_of_ev(&_19, php_pdo_get_exception())) {
			zend_clear_exception(TSRMLS_C);
			ZEPHIR_CPY_WRT(&e, &_19);
			zephir_array_fetch_string(&_20$$7, &option, SL("dsn"), PH_NOISY | PH_READONLY, "hellophp/Db.zep", 43);
			ZEPHIR_INIT_VAR(&_21$$7);
			ZVAL_STRING(&_21$$7, "connect to %s failed.");
			ZEPHIR_CALL_FUNCTION(&_22$$7, "sprintf", NULL, 5, &_21$$7, &_20$$7);
			zephir_check_call_status();
			ZEPHIR_CALL_CE_STATIC(NULL, hellophp_logger_ce, "log", &_15, 0, &_22$$7);
			zephir_check_call_status();
			ZEPHIR_INIT_NVAR(&_21$$7);
			object_init_ex(&_21$$7, hellophp_db_exception_ce);
			zephir_array_fetch_string(&_23$$7, &option, SL("dsn"), PH_NOISY | PH_READONLY, "hellophp/Db.zep", 44);
			ZEPHIR_CALL_METHOD(&_24$$7, &e, "getmessage", NULL, 0);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(&_25$$7);
			ZEPHIR_CONCAT_SVSV(&_25$$7, "Connect to db(", &_23$$7, ") failed: ", &_24$$7);
			ZVAL_LONG(&_26$$7, 102);
			ZEPHIR_CALL_METHOD(NULL, &_21$$7, "__construct", NULL, 6, &_25$$7, &_26$$7);
			zephir_check_call_status();
			zephir_throw_exception_debug(&_21$$7, "hellophp/Db.zep", 44);
			ZEPHIR_MM_RESTORE();
			return;
		}
	}
	zephir_update_static_property_array_multi_ce(hellophp_db_ce, SL("_instances"), &dbh, SL("z"), 1, instanceName);
	ZEPHIR_INIT_VAR(&_27);
	zephir_time(&_27);
	ZEPHIR_INIT_VAR(&_28);
	ZVAL_LONG(&_28, (zephir_get_numberval(&_27) + 60));
	zephir_update_static_property_array_multi_ce(hellophp_db_ce, SL("_instancesTimeout"), &_28, SL("z"), 1, instanceName);
	zephir_read_static_property_ce(&_29, hellophp_db_ce, SL("_instances"), PH_NOISY_CC | PH_READONLY);
	zephir_array_fetch(&_30, &_29, instanceName, PH_NOISY | PH_READONLY, "hellophp/Db.zep", 49);
	RETURN_CTOR(&_30);

}

PHP_METHOD(Hellophp_Db, query) {

	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
	zephir_fcall_cache_entry *_2 = NULL;
	zend_long ZEPHIR_LAST_CALL_STATUS;
	zval *query = NULL, query_sub, *params = NULL, params_sub, e, _7, stmt$$3, _0$$3, rows$$3, _1$$3, _3$$3, _4$$3, _5$$3, _6$$3, _8$$4, _9$$4, _10$$4, _11$$4, _12$$4, _13$$4;
	zval *this_ptr = getThis();

	ZVAL_UNDEF(&query_sub);
	ZVAL_UNDEF(&params_sub);
	ZVAL_UNDEF(&e);
	ZVAL_UNDEF(&_7);
	ZVAL_UNDEF(&stmt$$3);
	ZVAL_UNDEF(&_0$$3);
	ZVAL_UNDEF(&rows$$3);
	ZVAL_UNDEF(&_1$$3);
	ZVAL_UNDEF(&_3$$3);
	ZVAL_UNDEF(&_4$$3);
	ZVAL_UNDEF(&_5$$3);
	ZVAL_UNDEF(&_6$$3);
	ZVAL_UNDEF(&_8$$4);
	ZVAL_UNDEF(&_9$$4);
	ZVAL_UNDEF(&_10$$4);
	ZVAL_UNDEF(&_11$$4);
	ZVAL_UNDEF(&_12$$4);
	ZVAL_UNDEF(&_13$$4);

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 1, &query, &params);

	if (!params) {
		params = &params_sub;
		ZEPHIR_INIT_VAR(params);
		array_init(params);
	}



	/* try_start_1: */

		zephir_read_property(&_0$$3, this_ptr, ZEND_STRL("_dbh"), PH_NOISY_CC | PH_READONLY);
		ZEPHIR_CALL_METHOD(&stmt$$3, &_0$$3, "prepare", NULL, 0, query);
		zephir_check_call_status_or_jump(try_end_1);
		ZEPHIR_CALL_METHOD(NULL, &stmt$$3, "execute", NULL, 0, params);
		zephir_check_call_status_or_jump(try_end_1);
		ZVAL_LONG(&_1$$3, 2);
		ZEPHIR_CALL_METHOD(&rows$$3, &stmt$$3, "fetchall", NULL, 0, &_1$$3);
		zephir_check_call_status_or_jump(try_end_1);
		ZEPHIR_INIT_VAR(&_3$$3);
		zephir_json_encode(&_3$$3, params, 0 );
		ZEPHIR_INIT_VAR(&_4$$3);
		zephir_json_encode(&_4$$3, &rows$$3, 0 );
		ZEPHIR_INIT_VAR(&_5$$3);
		ZVAL_STRING(&_5$$3, "db query: %s (%s) => %s");
		ZEPHIR_CALL_FUNCTION(&_6$$3, "sprintf", NULL, 5, &_5$$3, query, &_3$$3, &_4$$3);
		zephir_check_call_status_or_jump(try_end_1);
		ZEPHIR_CALL_CE_STATIC(NULL, hellophp_logger_ce, "log", &_2, 0, &_6$$3);
		zephir_check_call_status_or_jump(try_end_1);
		RETURN_CCTOR(&rows$$3);

	try_end_1:

	if (EG(exception)) {
		ZEPHIR_INIT_VAR(&_7);
		ZVAL_OBJ(&_7, EG(exception));
		Z_ADDREF_P(&_7);
		if (zephir_instance_of_ev(&_7, php_pdo_get_exception())) {
			zend_clear_exception(TSRMLS_C);
			ZEPHIR_CPY_WRT(&e, &_7);
			ZEPHIR_INIT_VAR(&_8$$4);
			zephir_json_encode(&_8$$4, params, 0 );
			ZEPHIR_INIT_VAR(&_9$$4);
			ZVAL_STRING(&_9$$4, "db query failed. query=%s, params=%s");
			ZEPHIR_CALL_FUNCTION(&_10$$4, "sprintf", NULL, 5, &_9$$4, query, &_8$$4);
			zephir_check_call_status();
			ZEPHIR_CALL_CE_STATIC(NULL, hellophp_logger_ce, "log", &_2, 0, &_10$$4);
			zephir_check_call_status();
			ZEPHIR_INIT_NVAR(&_9$$4);
			object_init_ex(&_9$$4, hellophp_db_exception_ce);
			ZEPHIR_CALL_METHOD(&_11$$4, &e, "getmessage", NULL, 0);
			zephir_check_call_status();
			ZEPHIR_INIT_VAR(&_12$$4);
			ZEPHIR_CONCAT_SV(&_12$$4, "db query failed: ", &_11$$4);
			ZVAL_LONG(&_13$$4, 103);
			ZEPHIR_CALL_METHOD(NULL, &_9$$4, "__construct", NULL, 6, &_12$$4, &_13$$4);
			zephir_check_call_status();
			zephir_throw_exception_debug(&_9$$4, "hellophp/Db.zep", 65);
			ZEPHIR_MM_RESTORE();
			return;
		}
	}
	ZEPHIR_MM_RESTORE();

}

void zephir_init_static_properties_Hellophp_Db(TSRMLS_D) {

	zval _0, _1;
	zephir_method_globals *ZEPHIR_METHOD_GLOBALS_PTR = NULL;
		ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);

	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(&_0);
	array_init(&_0);
	zephir_update_static_property_ce(hellophp_db_ce, ZEND_STRL("_instancesTimeout"), &_0);
	ZEPHIR_INIT_VAR(&_1);
	array_init(&_1);
	zephir_update_static_property_ce(hellophp_db_ce, ZEND_STRL("_instances"), &_1);
	ZEPHIR_MM_RESTORE();

}

