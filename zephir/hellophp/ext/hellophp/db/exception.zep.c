
#ifdef HAVE_CONFIG_H
#include "../../ext_config.h"
#endif

#include <php.h>
#include "../../php_ext.h"
#include "../../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"


ZEPHIR_INIT_CLASS(Hellophp_Db_Exception) {

	ZEPHIR_REGISTER_CLASS_EX(Hellophp\\Db, Exception, hellophp, db_exception, zend_exception_get_default(TSRMLS_C), NULL, 0);

	zephir_declare_class_constant_long(hellophp_db_exception_ce, SL("DB_BAD_PARAM"), 101);

	zephir_declare_class_constant_long(hellophp_db_exception_ce, SL("DB_CONNECT_FAILED"), 102);

	zephir_declare_class_constant_long(hellophp_db_exception_ce, SL("DB_QUERY_FAILED"), 103);

	return SUCCESS;

}

