
extern zend_class_entry *hellophp_logger_ce;

ZEPHIR_INIT_CLASS(Hellophp_Logger);

PHP_METHOD(Hellophp_Logger, _getId);
PHP_METHOD(Hellophp_Logger, log);

ZEND_BEGIN_ARG_INFO_EX(arginfo_hellophp_logger_log, 0, 0, 1)
	ZEND_ARG_INFO(0, message)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(hellophp_logger_method_entry) {
	PHP_ME(Hellophp_Logger, _getId, NULL, ZEND_ACC_PRIVATE|ZEND_ACC_STATIC)
	PHP_ME(Hellophp_Logger, log, arginfo_hellophp_logger_log, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_FE_END
};
