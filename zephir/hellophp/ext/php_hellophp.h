
/* This file was generated automatically by Zephir do not modify it! */

#ifndef PHP_HELLOPHP_H
#define PHP_HELLOPHP_H 1

#ifdef PHP_WIN32
#define ZEPHIR_RELEASE 1
#endif

#include "kernel/globals.h"

#define PHP_HELLOPHP_NAME        "hellophp"
#define PHP_HELLOPHP_VERSION     "0.0.1"
#define PHP_HELLOPHP_EXTNAME     "hellophp"
#define PHP_HELLOPHP_AUTHOR      "dingusxp"
#define PHP_HELLOPHP_ZEPVERSION  "0.12.21-8a412a1"
#define PHP_HELLOPHP_DESCRIPTION "a test project with zephir"



ZEND_BEGIN_MODULE_GLOBALS(hellophp)

	int initialized;

	/** Function cache */
	HashTable *fcache;

	zephir_fcall_cache_entry *scache[ZEPHIR_MAX_CACHE_SLOTS];

	/* Cache enabled */
	unsigned int cache_enabled;

	/* Max recursion control */
	unsigned int recursive_lock;

	
ZEND_END_MODULE_GLOBALS(hellophp)

#ifdef ZTS
#include "TSRM.h"
#endif

ZEND_EXTERN_MODULE_GLOBALS(hellophp)

#ifdef ZTS
	#define ZEPHIR_GLOBAL(v) ZEND_MODULE_GLOBALS_ACCESSOR(hellophp, v)
#else
	#define ZEPHIR_GLOBAL(v) (hellophp_globals.v)
#endif

#ifdef ZTS
	ZEND_TSRMLS_CACHE_EXTERN()
	#define ZEPHIR_VGLOBAL ((zend_hellophp_globals *) (*((void ***) tsrm_get_ls_cache()))[TSRM_UNSHUFFLE_RSRC_ID(hellophp_globals_id)])
#else
	#define ZEPHIR_VGLOBAL &(hellophp_globals)
#endif

#define ZEPHIR_API ZEND_API

#define zephir_globals_def hellophp_globals
#define zend_zephir_globals_def zend_hellophp_globals

extern zend_module_entry hellophp_module_entry;
#define phpext_hellophp_ptr &hellophp_module_entry

#endif
