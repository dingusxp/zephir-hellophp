namespace Hellophp;

class Logger {

	private static function _getId() {
        
		if (!array_key_exists("HTTP_INVOKE_ID", _SERVER)) {
            var logId;
			let logId = sprintf("%10d%06d%03d", time(), getmypid(), mt_rand(0, 999));
			let _SERVER["HTTP_INVOKE_ID"] = logId;
		}
		return _SERVER["HTTP_INVOKE_ID"];
	}

    public static function log(message) {

        var date, microtime, content, logFile;
		let date = date("[Y-m-d H:i:s]");
		let microtime = strstr(microtime(true), ".");
		let content = sprintf("%s%s - [%s] %s\n", date, microtime, self::_getId(), message);
		let logFile = Config::get("log.dir", APP_PATH."/log")."/".date("Y-m-d").".log";
		if (!is_dir(dirname(logFile))) {
			mkdir(dirname(logFile), 0777, true);
		}
		file_put_contents(logFile, content, FILE_APPEND | LOCK_EX);
	}
}
