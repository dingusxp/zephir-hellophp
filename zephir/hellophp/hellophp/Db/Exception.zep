namespace Hellophp\Db;

class Exception extends \Exception {

    const DB_BAD_PARAM  = 101;
    const DB_CONNECT_FAILED = 102;
    const DB_QUERY_FAILED = 103;
}